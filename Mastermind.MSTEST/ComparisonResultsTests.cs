﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Mastermind.MSTEST
{
    [TestClass]
    public class ComparisonResultsTests
    {
        [TestMethod]
        public void BullsEye_Get()
        {
            //Arrange
            int BullsEyeInit = 4;
            int expected = 4;

            ComparisonResult comparisonResult = new ComparisonResult() { BullsEye = BullsEyeInit };
            var Actual = comparisonResult.BullsEye;

            Assert.AreEqual(expected, Actual);
        }

        [TestMethod]
        public void Hits_Get()
        {
            //Arrange
            int HitsInit = 4;
            int expected = 4;

            ComparisonResult comparisonResult = new ComparisonResult() { Hits = HitsInit };
            var Actual = comparisonResult.Hits;

            Assert.AreEqual(expected, Actual);
        }
    }
}
