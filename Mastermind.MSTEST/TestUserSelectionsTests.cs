﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Mastermind.MSTEST
{
    [TestClass]
    public class TestUserSelectionsTests
    {
        [TestMethod]
        public void Test_3BullsEye()
        {
            //Arrange
            ComparisonResult expected = new ComparisonResult() { BullsEye = 3, Hits = 0 };
            List<Color> inputCompColors = new List<Color>();
            inputCompColors.Add(Color.Red);
            inputCompColors.Add(Color.Blue);
            inputCompColors.Add(Color.Yellow);

            List<Color> inputUserColors = new List<Color>();
            inputUserColors.Add(Color.Red);
            inputUserColors.Add(Color.Blue);
            inputUserColors.Add(Color.Yellow);

            //Act
            TestUserSelections testUserSelections = new TestUserSelections();
            var actual = testUserSelections.Test(inputCompColors, inputUserColors);

            //Assert
            Assert.AreEqual(expected.BullsEye, actual.BullsEye);

        }


        [TestMethod]
        public void Test_1BullsEye2Hits()
        {
            //Arrange
            ComparisonResult expected = new ComparisonResult() { BullsEye = 1, Hits = 2 };
            List<Color> inputCompColors = new List<Color>();
            inputCompColors.Add(Color.Red);
            inputCompColors.Add(Color.Yellow);
            inputCompColors.Add(Color.Blue);

            List<Color> inputUserColors = new List<Color>();
            inputUserColors.Add(Color.Red);
            inputUserColors.Add(Color.Blue);
            inputUserColors.Add(Color.Yellow);

            //Act
            TestUserSelections testUserSelections = new TestUserSelections();
            var actual = testUserSelections.Test(inputCompColors, inputUserColors);

            //Assert
            Assert.IsTrue(expected.BullsEye == actual.BullsEye && expected.Hits == actual.Hits);

        }

        [TestMethod]
        public void Test_1BullsEye()
        {
            //Arrange
            ComparisonResult expected = new ComparisonResult() { BullsEye = 1, Hits = 0 };
            List<Color> inputCompColors = new List<Color>();
            inputCompColors.Add(Color.Red);
            inputCompColors.Add(Color.White);
            inputCompColors.Add(Color.Cyan);

            List<Color> inputUserColors = new List<Color>();
            inputUserColors.Add(Color.Red);
            inputUserColors.Add(Color.Blue);
            inputUserColors.Add(Color.Yellow);

            //Act
            TestUserSelections testUserSelections = new TestUserSelections();
            var actual = testUserSelections.Test(inputCompColors, inputUserColors);

            //Assert
            Assert.IsTrue(expected.BullsEye == actual.BullsEye && expected.Hits == actual.Hits);

        }

        [TestMethod]
        public void Test_0BullsEye0Hits()
        {
            //Arrange
            ComparisonResult expected = new ComparisonResult() { BullsEye = 0, Hits = 0 };
            List<Color> inputCompColors = new List<Color>();
            inputCompColors.Add(Color.Purple);
            inputCompColors.Add(Color.White);
            inputCompColors.Add(Color.Cyan);

            List<Color> inputUserColors = new List<Color>();
            inputUserColors.Add(Color.Red);
            inputUserColors.Add(Color.Blue);
            inputUserColors.Add(Color.Yellow);

            //Act
            TestUserSelections testUserSelections = new TestUserSelections();
            var actual = testUserSelections.Test(inputCompColors, inputUserColors);

            //Assert
            Assert.IsTrue(expected.BullsEye == actual.BullsEye && expected.Hits == actual.Hits);

        }


        [TestMethod]
        public void Test_0BullsEye3Hits()
        {
            //Arrange
            ComparisonResult expected = new ComparisonResult() { BullsEye = 0, Hits = 3 };
            List<Color> inputCompColors = new List<Color>();
            inputCompColors.Add(Color.Yellow);
            inputCompColors.Add(Color.Red);
            inputCompColors.Add(Color.Blue);

            List<Color> inputUserColors = new List<Color>();
            inputUserColors.Add(Color.Red);
            inputUserColors.Add(Color.Blue);
            inputUserColors.Add(Color.Yellow);

            //Act
            TestUserSelections testUserSelections = new TestUserSelections();
            var actual = testUserSelections.Test(inputCompColors, inputUserColors);

            //Assert
            Assert.IsTrue(expected.BullsEye == actual.BullsEye && expected.Hits == actual.Hits);

        }
    }
}
