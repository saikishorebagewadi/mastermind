﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Mastermind.MSTEST
{
    [TestClass]
    public class ColorDefinitionTests
    {
        [TestMethod]
        public void GetPossibleColors_ReturnTypeTest()
        {
            //Arrange
            Mastermind.ColorDefinition colorDefinition = new ColorDefinition();
            
            //Act
            var colordef = colorDefinition.GetPossibleColors();


            //Assert
            Assert.IsTrue(typeof(List<Color>) == colordef.GetType());


        }
    }
}
