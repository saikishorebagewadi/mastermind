﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Mastermind.MSTEST
{
    [TestClass]
    public class SelectComputerColorsTest
    {
        [TestMethod]
        public void Test_GetComputerColors_Count()
        {
            //Arrange
            int count = 3;
            int expectedCount = 3;
            List<Color> colors = new List<Color>();
            colors.Add(Color.Red);
            colors.Add(Color.Orange);
            colors.Add(Color.Yellow);
            colors.Add(Color.Green);
            colors.Add(Color.Blue);
            colors.Add(Color.Indigo);
            colors.Add(Color.Violet);

            //Act
            SelectComputerColors selectComputerColors = new SelectComputerColors();
            var result = selectComputerColors.GetComputerColors(colors, 3);

            //Assert
            Assert.AreEqual(expectedCount, result.Count);
        }

        [TestMethod]
        public void Test_GetComputerColors_validColors()
        {
            //Arrange
            int count = 3;
            int expectedCount = 3;
            int actualCount = 0;
            List<Color> colors = new List<Color>();
            colors.Add(Color.Red);
            colors.Add(Color.Orange);
            colors.Add(Color.Yellow);
            colors.Add(Color.Green);
            colors.Add(Color.Blue);
            colors.Add(Color.Indigo);
            colors.Add(Color.Violet);

            //Act
            SelectComputerColors selectComputerColors = new SelectComputerColors();
            var result = selectComputerColors.GetComputerColors(colors, 3);

            foreach(Color color in result)
            {
                if(colors.Contains(color))
                {
                    actualCount++;
                }
            }

            //Assert
            Assert.AreEqual(expectedCount, actualCount);
        }
        [TestMethod]
        public void Test_GetComputerColors_NoDups()
        {
            //Arrange
            int count = 3;
            int expectedCount = 3;
            int actualCount = 0;
            List<Color> colors = new List<Color>();
            colors.Add(Color.Red);
            colors.Add(Color.Orange);
            colors.Add(Color.Yellow);
            colors.Add(Color.Green);
            colors.Add(Color.Blue);
            colors.Add(Color.Indigo);
            colors.Add(Color.Violet);

            //Act
            SelectComputerColors selectComputerColors = new SelectComputerColors();
            var result = selectComputerColors.GetComputerColors(colors, 3);

           actualCount= result.Distinct().Count();

            //Assert
            Assert.AreEqual(expectedCount, actualCount);
        }
    }
}
