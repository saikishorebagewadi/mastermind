﻿using System.Collections.Generic;
using System.Drawing;

namespace Mastermind
{
    public interface ITestUserSelections
    {
        ComparisonResult Test(List<Color> compColors, List<Color> userColors);
    }
}