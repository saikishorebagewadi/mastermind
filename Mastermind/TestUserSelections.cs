﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Mastermind
{
    public class TestUserSelections : ITestUserSelections
    {
        public ComparisonResult Test(List<Color> compColors, List<Color> userColors)
        {
            ComparisonResult comparisonResult = new ComparisonResult();

            for (int i = 0; i < userColors.Count; i++)
            {
                if (userColors[i] == compColors[i])
                {
                    comparisonResult.BullsEye++;
                    //  ((Panel)container.Controls.Find(container.Name + "_status" + (i + 1), true).First()).BackColor = Color.Black;
                }
                else if (compColors.Contains(userColors[i]))
                {
                    comparisonResult.Hits++;
                    //  ((Panel)container.Controls.Find(container.Name + "_status" + (i + 1), true).First()).BackColor = Color.Red;
                }
            }

            return comparisonResult;
        }
    }
}
