﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastermind
{
    public class SelectComputerColors : ISelectComputerColors
    {
        public List<Color> GetComputerColors(List<Color> colors, int count)
        {
            List<Color> compcolors = new List<Color>();
            Random rand = new Random();
            Color randomColor = new Color();
            while (compcolors.Count < count)
            {
                randomColor = colors[rand.Next(colors.Count)];
                if (!compcolors.Contains(randomColor))
                {
                    compcolors.Add(randomColor);
                }
            }
            return compcolors;
        }
    }
}
