﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mastermind
{
    public partial class FrmDecodingBoard : Form
    {
        List<Color> posscolors = new List<Color>();
        List<Color> compColors = new List<Color>();
        private ITestUserSelections _testUserSelections;
        private ISelectComputerColors _selectComputerColors;

        public FrmDecodingBoard(IColorDefinition colorDefinition, ITestUserSelections testUserSelections, ISelectComputerColors selectComputerColors)
        {
            InitializeComponent();
            posscolors = colorDefinition.GetPossibleColors();
            _testUserSelections = testUserSelections;
            _selectComputerColors = selectComputerColors;
        }
         
        private void btnTest_Click(object sender, System.EventArgs e)
        {
            Button btn = (Button)sender;
            List<Color> userColors = new List<Color>();
           Panel container = (Panel) btn.Parent;
           userColors.Add(((Panel)container.Controls.Find(container.Name + "_Color1", true).First()).BackColor);
           userColors.Add(((Panel)container.Controls.Find(container.Name + "_Color2", true).First()).BackColor);
           userColors.Add(((Panel)container.Controls.Find(container.Name + "_Color3", true).First()).BackColor);
           int bullseye = 0;
           int hits = 0;
           
            //Dependency Inversion Principle mandates to push such dependencies to calling class.
           // TestUserSelections testUserSelections = new TestUserSelections();
            var result = _testUserSelections.Test(compColors, userColors);

            bullseye = result.BullsEye;
            hits = result.Hits;
        //Coloring the status display
           for (int i = 0; i < 3; i++)
           {
               if (bullseye > 0)
               {
                   ((Panel)container.Controls.Find(container.Name + "_status" + (i + 1), true).First()).BackColor = Color.Black;
                   bullseye--;
               }
               else if (hits > 0)
               {
                   ((Panel)container.Controls.Find(container.Name + "_status" + (i + 1), true).First()).BackColor = Color.Red;
                   hits--;
               }
           }

          int Trialcount = Convert.ToInt32(container.Name.Substring(3, 1));
          if (Trialcount == 6 || result.BullsEye == 3)
          {
              FrmResult _frmresult;
              if (result.BullsEye == 3)
              {
                    _frmresult = new FrmResult(true);
                
              }
              else
              {
                    _frmresult = new FrmResult(false);
              }
              if (!pnl0.Visible)
              {
                  //show Computer selection to user finally
                  btnShowHide_Click(null, null);
              }
              if (_frmresult.ShowDialog() == DialogResult.Cancel)
              {
                  Application.Restart();
              }
          }
          else
          {
              ((Panel)this.Controls.Find("pnl" + (Trialcount + 1), true).First()).Show();
          }
        }

        private void btnShowHide_Click(object sender, EventArgs e)
        {
            if (pnl0.Visible)
            {
                pnl0.Hide();
                btnShowHide.Text = "&Show";
            }
            else
            {
                pnl0.Show();
                btnShowHide.Text = "&Hide";
            }
          

        }

        private void FrmDecodingBoard_Load(object sender, EventArgs e)
        {
             
            //Initalize colors
            SetDefaults();
            //Dependency Inversion Principle mandates to push such dependencies to calling class.
            //SelectComputerColors selectComputerColors = new SelectComputerColors();
            compColors = _selectComputerColors.GetComputerColors(posscolors, 3);
           
            setComputerpnlkColors();

        }

        private void setComputerpnlkColors()
        {

            pnl0_Color1.BackColor = compColors[0];
            pnl0_Color2.BackColor = compColors[1];
            pnl0_Color3.BackColor = compColors[2];
          
        }
       
        private void SetDefaults()
        {
             btnShowHide_Click(null, null);


           // Dependency Inversion principle mandates to push all such dependencies out of class.
            //ColorDefinition colorDefinition = new ColorDefinition();
            //posscolors = colorDefinition.GetPossibleColors();


            //HideAll Panels
            HideAllPanels();
            pnl1.Show();
   
        }

        private void HideAllPanels()
        {
       
            pnl1.Visible = false;
            pnl2.Visible = false;
            pnl3.Visible = false;
            pnl4.Visible = false;
            pnl5.Visible = false;
            pnl6.Visible = false;

           
        }

        private void pnlColor_Click(object sender, EventArgs e)
        {
                       Panel pnl = (Panel)sender;
                       if (pnl.BackColor.IsSystemColor)
                       {
                           Random rand = new Random();
                           pnl.BackColor = posscolors[rand.Next(posscolors.Count)];
                       }
                       else
                       {
                           pnl.BackColor = posscolors[(posscolors.IndexOf(pnl.BackColor) + 1) % posscolors.Count];
                       }
        }

        private void pnl_Color_MouseHover(object sender, EventArgs e)
        {
            ToolTip buttonToolTip = new ToolTip();
       
            buttonToolTip.UseFading = true;
            buttonToolTip.UseAnimation = true;
            buttonToolTip.IsBalloon = true;
            buttonToolTip.ToolTipIcon = ToolTipIcon.Info;
            buttonToolTip.ShowAlways = true;

            if (sender is Button)
            {
                buttonToolTip.ToolTipTitle = "information";
                buttonToolTip.SetToolTip((Control)sender, "Click to test your guess.");
            }
            else
            {
                if (((Control)sender).Name.Contains("Color"))
                {
                    buttonToolTip.ToolTipTitle = "information";
                    buttonToolTip.SetToolTip((Control)sender, "Click on the code peg to change its color. The Sequence is ROYGBIV");
                }
                if (((Control)sender).Name.Contains("Status"))
                {
                    buttonToolTip.ToolTipTitle = "information";
                    buttonToolTip.SetToolTip((Control)sender, "Black-position and color are correct.\nRed-correct color but in incorrect position");
                }
            }

            
        }


    
    }
}
