﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastermind
{
    public class ColorDefinition : IColorDefinition
    {

        public List<Color> GetPossibleColors()
        {

            List<Color> posscolors = new List<Color>();
            posscolors.Add(Color.Red);
            posscolors.Add(Color.Orange);
            posscolors.Add(Color.Yellow);
            posscolors.Add(Color.Green);
            posscolors.Add(Color.Blue);
            posscolors.Add(Color.Indigo);
            posscolors.Add(Color.Violet);


            return posscolors;
        }
    }
}
