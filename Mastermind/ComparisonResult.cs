﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastermind
{
   public class ComparisonResult
    {
        public int BullsEye { get; set; }
        public int Hits { get; set; }
    }
}
