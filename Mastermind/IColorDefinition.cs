﻿using System.Collections.Generic;
using System.Drawing;

namespace Mastermind
{
    public interface IColorDefinition
    {
        List<Color> GetPossibleColors();
    }
}