﻿using System.Collections.Generic;
using System.Drawing;

namespace Mastermind
{
    public interface ISelectComputerColors
    {
        List<Color> GetComputerColors(List<Color> colors, int count);
    }
}