﻿namespace Mastermind
{
    partial class FrmDecodingBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        //should we change the designer
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl6 = new System.Windows.Forms.Panel();
            this.pnl6_Status3 = new System.Windows.Forms.Panel();
            this.pnl6_Status2 = new System.Windows.Forms.Panel();
            this.pnl6_Status1 = new System.Windows.Forms.Panel();
            this.pnl6_Color1 = new System.Windows.Forms.Panel();
            this.pnl6_btnTest = new System.Windows.Forms.Button();
            this.pnl6_Color3 = new System.Windows.Forms.Panel();
            this.pnl6_Color2 = new System.Windows.Forms.Panel();
            this.btnShowHide = new System.Windows.Forms.Button();
            this.pnlSeperator = new System.Windows.Forms.Panel();
            this.pnl0 = new System.Windows.Forms.Panel();
            this.pnl0_Color1 = new System.Windows.Forms.Panel();
            this.pnl0_Color3 = new System.Windows.Forms.Panel();
            this.pnl0_Color2 = new System.Windows.Forms.Panel();
            this.pnl5 = new System.Windows.Forms.Panel();
            this.pnl5_status3 = new System.Windows.Forms.Panel();
            this.pnl5_Status2 = new System.Windows.Forms.Panel();
            this.pnl5_Status1 = new System.Windows.Forms.Panel();
            this.pnl5_Color1 = new System.Windows.Forms.Panel();
            this.pnl5_btnTest = new System.Windows.Forms.Button();
            this.pnl5_Color3 = new System.Windows.Forms.Panel();
            this.pnl5_Color2 = new System.Windows.Forms.Panel();
            this.pnl3 = new System.Windows.Forms.Panel();
            this.pnl3_Status3 = new System.Windows.Forms.Panel();
            this.pnl3_Status2 = new System.Windows.Forms.Panel();
            this.pnl3_Status1 = new System.Windows.Forms.Panel();
            this.pnl3_Color1 = new System.Windows.Forms.Panel();
            this.pnl3_btnTest = new System.Windows.Forms.Button();
            this.pnl3_Color3 = new System.Windows.Forms.Panel();
            this.pnl3_Color2 = new System.Windows.Forms.Panel();
            this.pnl4 = new System.Windows.Forms.Panel();
            this.pnl4_Status3 = new System.Windows.Forms.Panel();
            this.pnl4_Status2 = new System.Windows.Forms.Panel();
            this.pnl4_Status1 = new System.Windows.Forms.Panel();
            this.pnl4_Color1 = new System.Windows.Forms.Panel();
            this.pnl4_btnTest = new System.Windows.Forms.Button();
            this.pnl4_Color3 = new System.Windows.Forms.Panel();
            this.pnl4_Color2 = new System.Windows.Forms.Panel();
            this.pnl1 = new System.Windows.Forms.Panel();
            this.pnl1_Status3 = new System.Windows.Forms.Panel();
            this.pnl1_Status2 = new System.Windows.Forms.Panel();
            this.pnl1_Status1 = new System.Windows.Forms.Panel();
            this.pnl1_Color1 = new System.Windows.Forms.Panel();
            this.pnl1_btnTest = new System.Windows.Forms.Button();
            this.pnl1_Color3 = new System.Windows.Forms.Panel();
            this.pnl1_Color2 = new System.Windows.Forms.Panel();
            this.pnl2 = new System.Windows.Forms.Panel();
            this.pnl2_Status3 = new System.Windows.Forms.Panel();
            this.pnl2_Status2 = new System.Windows.Forms.Panel();
            this.pnl2_Status1 = new System.Windows.Forms.Panel();
            this.pnl2_Color1 = new System.Windows.Forms.Panel();
            this.pnl2_btnTest = new System.Windows.Forms.Button();
            this.pnl2_Color3 = new System.Windows.Forms.Panel();
            this.pnl2_Color2 = new System.Windows.Forms.Panel();
            this.pnl6.SuspendLayout();
            this.pnl0.SuspendLayout();
            this.pnl5.SuspendLayout();
            this.pnl3.SuspendLayout();
            this.pnl4.SuspendLayout();
            this.pnl1.SuspendLayout();
            this.pnl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl6
            // 
            this.pnl6.Controls.Add(this.pnl6_Status3);
            this.pnl6.Controls.Add(this.pnl6_Status2);
            this.pnl6.Controls.Add(this.pnl6_Status1);
            this.pnl6.Controls.Add(this.pnl6_Color1);
            this.pnl6.Controls.Add(this.pnl6_btnTest);
            this.pnl6.Controls.Add(this.pnl6_Color3);
            this.pnl6.Controls.Add(this.pnl6_Color2);
            this.pnl6.Location = new System.Drawing.Point(12, 12);
            this.pnl6.Name = "pnl6";
            this.pnl6.Size = new System.Drawing.Size(341, 74);
            this.pnl6.TabIndex = 0;
            // 
            // pnl6_Status3
            // 
            this.pnl6_Status3.Location = new System.Drawing.Point(297, 45);
            this.pnl6_Status3.Name = "pnl6_Status3";
            this.pnl6_Status3.Size = new System.Drawing.Size(22, 19);
            this.pnl6_Status3.TabIndex = 6;
  
            this.pnl6_Status3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl6_Status2
            // 
            this.pnl6_Status2.Location = new System.Drawing.Point(269, 45);
            this.pnl6_Status2.Name = "pnl6_Status2";
            this.pnl6_Status2.Size = new System.Drawing.Size(22, 19);
            this.pnl6_Status2.TabIndex = 5;
          
            this.pnl6_Status2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl6_Status1
            // 
            this.pnl6_Status1.Location = new System.Drawing.Point(241, 45);
            this.pnl6_Status1.Name = "pnl6_Status1";
            this.pnl6_Status1.Size = new System.Drawing.Size(22, 19);
            this.pnl6_Status1.TabIndex = 4;
   
            this.pnl6_Status1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl6_Color1
            // 
            this.pnl6_Color1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl6_Color1.Location = new System.Drawing.Point(9, 16);
            this.pnl6_Color1.Name = "pnl6_Color1";
            this.pnl6_Color1.Size = new System.Drawing.Size(58, 47);
            this.pnl6_Color1.TabIndex = 1;
            this.pnl6_Color1.Click += new System.EventHandler(this.pnlColor_Click);
        
            this.pnl6_Color1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl6_btnTest
            // 
            this.pnl6_btnTest.Location = new System.Drawing.Point(241, 16);
            this.pnl6_btnTest.Name = "pnl6_btnTest";
            this.pnl6_btnTest.Size = new System.Drawing.Size(74, 23);
            this.pnl6_btnTest.TabIndex = 7;
            this.pnl6_btnTest.Text = "Test";
            this.pnl6_btnTest.UseVisualStyleBackColor = true;
            this.pnl6_btnTest.Click += new System.EventHandler(this.btnTest_Click);
            this.pnl6_btnTest.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl6_Color3
            // 
            this.pnl6_Color3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl6_Color3.Location = new System.Drawing.Point(137, 16);
            this.pnl6_Color3.Name = "pnl6_Color3";
            this.pnl6_Color3.Size = new System.Drawing.Size(58, 47);
            this.pnl6_Color3.TabIndex = 3;
            this.pnl6_Color3.Click += new System.EventHandler(this.pnlColor_Click);
      
            this.pnl6_Color3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl6_Color2
            // 
            this.pnl6_Color2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl6_Color2.Location = new System.Drawing.Point(73, 16);
            this.pnl6_Color2.Name = "pnl6_Color2";
            this.pnl6_Color2.Size = new System.Drawing.Size(58, 47);
            this.pnl6_Color2.TabIndex = 2;
            this.pnl6_Color2.Click += new System.EventHandler(this.pnlColor_Click);

            this.pnl6_Color2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // btnShowHide
            // 
            this.btnShowHide.Location = new System.Drawing.Point(279, 590);
            this.btnShowHide.Name = "btnShowHide";
            this.btnShowHide.Size = new System.Drawing.Size(74, 43);
            this.btnShowHide.TabIndex = 1;
            this.btnShowHide.Text = "&Hide";
            this.btnShowHide.UseVisualStyleBackColor = true;
            this.btnShowHide.Click += new System.EventHandler(this.btnShowHide_Click);
            // 
            // pnlSeperator
            // 
            this.pnlSeperator.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pnlSeperator.Location = new System.Drawing.Point(6, 556);
            this.pnlSeperator.Name = "pnlSeperator";
            this.pnlSeperator.Size = new System.Drawing.Size(353, 10);
            this.pnlSeperator.TabIndex = 7;
            // 
            // pnl0
            // 
            this.pnl0.Controls.Add(this.pnl0_Color1);
            this.pnl0.Controls.Add(this.pnl0_Color3);
            this.pnl0.Controls.Add(this.pnl0_Color2);
            this.pnl0.Location = new System.Drawing.Point(21, 573);
            this.pnl0.Name = "pnl0";
            this.pnl0.Size = new System.Drawing.Size(212, 80);
            this.pnl0.TabIndex = 8;
            // 
            // pnl0_Color1
            // 
            this.pnl0_Color1.Location = new System.Drawing.Point(9, 16);
            this.pnl0_Color1.Name = "pnl0_Color1";
            this.pnl0_Color1.Size = new System.Drawing.Size(58, 47);
            this.pnl0_Color1.TabIndex = 2;
            // 
            // pnl0_Color3
            // 
            this.pnl0_Color3.Location = new System.Drawing.Point(137, 16);
            this.pnl0_Color3.Name = "pnl0_Color3";
            this.pnl0_Color3.Size = new System.Drawing.Size(58, 47);
            this.pnl0_Color3.TabIndex = 8;
            // 
            // pnl0_Color2
            // 
            this.pnl0_Color2.Location = new System.Drawing.Point(73, 16);
            this.pnl0_Color2.Name = "pnl0_Color2";
            this.pnl0_Color2.Size = new System.Drawing.Size(58, 47);
            this.pnl0_Color2.TabIndex = 3;
            // 
            // pnl5
            // 
            this.pnl5.Controls.Add(this.pnl5_status3);
            this.pnl5.Controls.Add(this.pnl5_Status2);
            this.pnl5.Controls.Add(this.pnl5_Status1);
            this.pnl5.Controls.Add(this.pnl5_Color1);
            this.pnl5.Controls.Add(this.pnl5_btnTest);
            this.pnl5.Controls.Add(this.pnl5_Color3);
            this.pnl5.Controls.Add(this.pnl5_Color2);
            this.pnl5.Location = new System.Drawing.Point(12, 98);
            this.pnl5.Name = "pnl5";
            this.pnl5.Size = new System.Drawing.Size(341, 74);
            this.pnl5.TabIndex = 11;
  
            // 
            // pnl5_status3
            // 
            this.pnl5_status3.Location = new System.Drawing.Point(297, 45);
            this.pnl5_status3.Name = "pnl5_status3";
            this.pnl5_status3.Size = new System.Drawing.Size(22, 19);
            this.pnl5_status3.TabIndex = 10;
            this.pnl5_status3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl5_Status2
            // 
            this.pnl5_Status2.Location = new System.Drawing.Point(269, 45);
            this.pnl5_Status2.Name = "pnl5_Status2";
            this.pnl5_Status2.Size = new System.Drawing.Size(22, 19);
            this.pnl5_Status2.TabIndex = 9;
            this.pnl5_Status2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl5_Status1
            // 
            this.pnl5_Status1.Location = new System.Drawing.Point(241, 45);
            this.pnl5_Status1.Name = "pnl5_Status1";
            this.pnl5_Status1.Size = new System.Drawing.Size(22, 19);
            this.pnl5_Status1.TabIndex = 8;
            this.pnl5_Status1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl5_Color1
            // 
            this.pnl5_Color1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl5_Color1.Location = new System.Drawing.Point(9, 16);
            this.pnl5_Color1.Name = "pnl5_Color1";
            this.pnl5_Color1.Size = new System.Drawing.Size(58, 47);
            this.pnl5_Color1.TabIndex = 2;
            this.pnl5_Color1.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl5_Color1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl5_btnTest
            // 
            this.pnl5_btnTest.Location = new System.Drawing.Point(241, 16);
            this.pnl5_btnTest.Name = "pnl5_btnTest";
            this.pnl5_btnTest.Size = new System.Drawing.Size(74, 23);
            this.pnl5_btnTest.TabIndex = 1;
            this.pnl5_btnTest.Text = "Test";
            this.pnl5_btnTest.UseVisualStyleBackColor = true;
            this.pnl5_btnTest.Click += new System.EventHandler(this.btnTest_Click);
            this.pnl5_btnTest.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl5_Color3
            // 
            this.pnl5_Color3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl5_Color3.Location = new System.Drawing.Point(137, 16);
            this.pnl5_Color3.Name = "pnl5_Color3";
            this.pnl5_Color3.Size = new System.Drawing.Size(58, 47);
            this.pnl5_Color3.TabIndex = 8;
            this.pnl5_Color3.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl5_Color3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl5_Color2
            // 
            this.pnl5_Color2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl5_Color2.Location = new System.Drawing.Point(73, 16);
            this.pnl5_Color2.Name = "pnl5_Color2";
            this.pnl5_Color2.Size = new System.Drawing.Size(58, 47);
            this.pnl5_Color2.TabIndex = 3;
            this.pnl5_Color2.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl5_Color2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl3
            // 
            this.pnl3.Controls.Add(this.pnl3_Status3);
            this.pnl3.Controls.Add(this.pnl3_Status2);
            this.pnl3.Controls.Add(this.pnl3_Status1);
            this.pnl3.Controls.Add(this.pnl3_Color1);
            this.pnl3.Controls.Add(this.pnl3_btnTest);
            this.pnl3.Controls.Add(this.pnl3_Color3);
            this.pnl3.Controls.Add(this.pnl3_Color2);
            this.pnl3.Location = new System.Drawing.Point(12, 270);
            this.pnl3.Name = "pnl3";
            this.pnl3.Size = new System.Drawing.Size(341, 74);
            this.pnl3.TabIndex = 13;
            // 
            // pnl3_Status3
            // 
            this.pnl3_Status3.Location = new System.Drawing.Point(297, 45);
            this.pnl3_Status3.Name = "pnl3_Status3";
            this.pnl3_Status3.Size = new System.Drawing.Size(22, 19);
            this.pnl3_Status3.TabIndex = 10;
            this.pnl3_Status3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl3_Status2
            // 
            this.pnl3_Status2.Location = new System.Drawing.Point(269, 45);
            this.pnl3_Status2.Name = "pnl3_Status2";
            this.pnl3_Status2.Size = new System.Drawing.Size(22, 19);
            this.pnl3_Status2.TabIndex = 9;
            this.pnl3_Status2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl3_Status1
            // 
            this.pnl3_Status1.Location = new System.Drawing.Point(241, 45);
            this.pnl3_Status1.Name = "pnl3_Status1";
            this.pnl3_Status1.Size = new System.Drawing.Size(22, 19);
            this.pnl3_Status1.TabIndex = 8;
            this.pnl3_Status1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl3_Color1
            // 
            this.pnl3_Color1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl3_Color1.Location = new System.Drawing.Point(9, 16);
            this.pnl3_Color1.Name = "pnl3_Color1";
            this.pnl3_Color1.Size = new System.Drawing.Size(58, 47);
            this.pnl3_Color1.TabIndex = 2;
            this.pnl3_Color1.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl3_Color1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl3_btnTest
            // 
            this.pnl3_btnTest.Location = new System.Drawing.Point(241, 16);
            this.pnl3_btnTest.Name = "pnl3_btnTest";
            this.pnl3_btnTest.Size = new System.Drawing.Size(74, 23);
            this.pnl3_btnTest.TabIndex = 1;
            this.pnl3_btnTest.Text = "Test";
            this.pnl3_btnTest.UseVisualStyleBackColor = true;
            this.pnl3_btnTest.Click += new System.EventHandler(this.btnTest_Click);
            this.pnl3_btnTest.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl3_Color3
            // 
            this.pnl3_Color3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl3_Color3.Location = new System.Drawing.Point(137, 16);
            this.pnl3_Color3.Name = "pnl3_Color3";
            this.pnl3_Color3.Size = new System.Drawing.Size(58, 47);
            this.pnl3_Color3.TabIndex = 8;
            this.pnl3_Color3.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl3_Color3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl3_Color2
            // 
            this.pnl3_Color2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl3_Color2.Location = new System.Drawing.Point(73, 16);
            this.pnl3_Color2.Name = "pnl3_Color2";
            this.pnl3_Color2.Size = new System.Drawing.Size(58, 47);
            this.pnl3_Color2.TabIndex = 3;
            this.pnl3_Color2.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl3_Color2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl4
            // 
            this.pnl4.Controls.Add(this.pnl4_Status3);
            this.pnl4.Controls.Add(this.pnl4_Status2);
            this.pnl4.Controls.Add(this.pnl4_Status1);
            this.pnl4.Controls.Add(this.pnl4_Color1);
            this.pnl4.Controls.Add(this.pnl4_btnTest);
            this.pnl4.Controls.Add(this.pnl4_Color3);
            this.pnl4.Controls.Add(this.pnl4_Color2);
            this.pnl4.Location = new System.Drawing.Point(12, 184);
            this.pnl4.Name = "pnl4";
            this.pnl4.Size = new System.Drawing.Size(341, 74);
            this.pnl4.TabIndex = 12;
            // 
            // pnl4_Status3
            // 
            this.pnl4_Status3.Location = new System.Drawing.Point(297, 45);
            this.pnl4_Status3.Name = "pnl4_Status3";
            this.pnl4_Status3.Size = new System.Drawing.Size(22, 19);
            this.pnl4_Status3.TabIndex = 10;
            this.pnl4_Status3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl4_Status2
            // 
            this.pnl4_Status2.Location = new System.Drawing.Point(269, 45);
            this.pnl4_Status2.Name = "pnl4_Status2";
            this.pnl4_Status2.Size = new System.Drawing.Size(22, 19);
            this.pnl4_Status2.TabIndex = 9;
            this.pnl4_Status2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl4_Status1
            // 
            this.pnl4_Status1.Location = new System.Drawing.Point(241, 45);
            this.pnl4_Status1.Name = "pnl4_Status1";
            this.pnl4_Status1.Size = new System.Drawing.Size(22, 19);
            this.pnl4_Status1.TabIndex = 8;
            this.pnl4_Status1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl4_Color1
            // 
            this.pnl4_Color1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl4_Color1.Location = new System.Drawing.Point(9, 16);
            this.pnl4_Color1.Name = "pnl4_Color1";
            this.pnl4_Color1.Size = new System.Drawing.Size(58, 47);
            this.pnl4_Color1.TabIndex = 2;
            this.pnl4_Color1.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl4_Color1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl4_btnTest
            // 
            this.pnl4_btnTest.Location = new System.Drawing.Point(241, 16);
            this.pnl4_btnTest.Name = "pnl4_btnTest";
            this.pnl4_btnTest.Size = new System.Drawing.Size(74, 23);
            this.pnl4_btnTest.TabIndex = 1;
            this.pnl4_btnTest.Text = "Test";
            this.pnl4_btnTest.UseVisualStyleBackColor = true;
            this.pnl4_btnTest.Click += new System.EventHandler(this.btnTest_Click);
            this.pnl4_btnTest.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl4_Color3
            // 
            this.pnl4_Color3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl4_Color3.Location = new System.Drawing.Point(137, 16);
            this.pnl4_Color3.Name = "pnl4_Color3";
            this.pnl4_Color3.Size = new System.Drawing.Size(58, 47);
            this.pnl4_Color3.TabIndex = 8;
            this.pnl4_Color3.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl4_Color3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl4_Color2
            // 
            this.pnl4_Color2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl4_Color2.Location = new System.Drawing.Point(73, 16);
            this.pnl4_Color2.Name = "pnl4_Color2";
            this.pnl4_Color2.Size = new System.Drawing.Size(58, 47);
            this.pnl4_Color2.TabIndex = 3;
            this.pnl4_Color2.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl4_Color2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl1
            // 
            this.pnl1.Controls.Add(this.pnl1_Status3);
            this.pnl1.Controls.Add(this.pnl1_Status2);
            this.pnl1.Controls.Add(this.pnl1_Status1);
            this.pnl1.Controls.Add(this.pnl1_Color1);
            this.pnl1.Controls.Add(this.pnl1_btnTest);
            this.pnl1.Controls.Add(this.pnl1_Color3);
            this.pnl1.Controls.Add(this.pnl1_Color2);
            this.pnl1.Location = new System.Drawing.Point(12, 442);
            this.pnl1.Name = "pnl1";
            this.pnl1.Size = new System.Drawing.Size(341, 74);
            this.pnl1.TabIndex = 15;
            // 
            // pnl1_Status3
            // 
            this.pnl1_Status3.Location = new System.Drawing.Point(297, 45);
            this.pnl1_Status3.Name = "pnl1_Status3";
            this.pnl1_Status3.Size = new System.Drawing.Size(22, 19);
            this.pnl1_Status3.TabIndex = 10;
            this.pnl1_Status3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl1_Status2
            // 
            this.pnl1_Status2.Location = new System.Drawing.Point(269, 45);
            this.pnl1_Status2.Name = "pnl1_Status2";
            this.pnl1_Status2.Size = new System.Drawing.Size(22, 19);
            this.pnl1_Status2.TabIndex = 9;
            this.pnl1_Status2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl1_Status1
            // 
            this.pnl1_Status1.Location = new System.Drawing.Point(241, 45);
            this.pnl1_Status1.Name = "pnl1_Status1";
            this.pnl1_Status1.Size = new System.Drawing.Size(22, 19);
            this.pnl1_Status1.TabIndex = 8;
            this.pnl1_Status1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl1_Color1
            // 
            this.pnl1_Color1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl1_Color1.Location = new System.Drawing.Point(9, 16);
            this.pnl1_Color1.Name = "pnl1_Color1";
            this.pnl1_Color1.Size = new System.Drawing.Size(58, 47);
            this.pnl1_Color1.TabIndex = 2;
            this.pnl1_Color1.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl1_Color1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl1_btnTest
            // 
            this.pnl1_btnTest.Location = new System.Drawing.Point(241, 16);
            this.pnl1_btnTest.Name = "pnl1_btnTest";
            this.pnl1_btnTest.Size = new System.Drawing.Size(74, 23);
            this.pnl1_btnTest.TabIndex = 1;
            this.pnl1_btnTest.Text = "Test";
            this.pnl1_btnTest.UseVisualStyleBackColor = true;
            this.pnl1_btnTest.Click += new System.EventHandler(this.btnTest_Click);
            this.pnl1_btnTest.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl1_Color3
            // 
            this.pnl1_Color3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl1_Color3.Location = new System.Drawing.Point(137, 16);
            this.pnl1_Color3.Name = "pnl1_Color3";
            this.pnl1_Color3.Size = new System.Drawing.Size(58, 47);
            this.pnl1_Color3.TabIndex = 8;
            this.pnl1_Color3.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl1_Color3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl1_Color2
            // 
            this.pnl1_Color2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl1_Color2.Location = new System.Drawing.Point(73, 16);
            this.pnl1_Color2.Name = "pnl1_Color2";
            this.pnl1_Color2.Size = new System.Drawing.Size(58, 47);
            this.pnl1_Color2.TabIndex = 3;
            this.pnl1_Color2.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl1_Color2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl2
            // 
            this.pnl2.Controls.Add(this.pnl2_Status3);
            this.pnl2.Controls.Add(this.pnl2_Status2);
            this.pnl2.Controls.Add(this.pnl2_Status1);
            this.pnl2.Controls.Add(this.pnl2_Color1);
            this.pnl2.Controls.Add(this.pnl2_btnTest);
            this.pnl2.Controls.Add(this.pnl2_Color3);
            this.pnl2.Controls.Add(this.pnl2_Color2);
            this.pnl2.Location = new System.Drawing.Point(12, 356);
            this.pnl2.Name = "pnl2";
            this.pnl2.Size = new System.Drawing.Size(341, 74);
            this.pnl2.TabIndex = 14;
            // 
            // pnl2_Status3
            // 
            this.pnl2_Status3.Location = new System.Drawing.Point(297, 45);
            this.pnl2_Status3.Name = "pnl2_Status3";
            this.pnl2_Status3.Size = new System.Drawing.Size(22, 19);
            this.pnl2_Status3.TabIndex = 10;
            this.pnl2_Status3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl2_Status2
            // 
            this.pnl2_Status2.Location = new System.Drawing.Point(269, 45);
            this.pnl2_Status2.Name = "pnl2_Status2";
            this.pnl2_Status2.Size = new System.Drawing.Size(22, 19);
            this.pnl2_Status2.TabIndex = 9;
            this.pnl2_Status2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl2_Status1
            // 
            this.pnl2_Status1.Location = new System.Drawing.Point(241, 45);
            this.pnl2_Status1.Name = "pnl2_Status1";
            this.pnl2_Status1.Size = new System.Drawing.Size(22, 19);
            this.pnl2_Status1.TabIndex = 8;
            this.pnl2_Status1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl2_Color1
            // 
            this.pnl2_Color1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl2_Color1.Location = new System.Drawing.Point(9, 16);
            this.pnl2_Color1.Name = "pnl2_Color1";
            this.pnl2_Color1.Size = new System.Drawing.Size(58, 47);
            this.pnl2_Color1.TabIndex = 2;
            this.pnl2_Color1.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl2_Color1.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl2_btnTest
            // 
            this.pnl2_btnTest.Location = new System.Drawing.Point(241, 16);
            this.pnl2_btnTest.Name = "pnl2_btnTest";
            this.pnl2_btnTest.Size = new System.Drawing.Size(74, 23);
            this.pnl2_btnTest.TabIndex = 1;
            this.pnl2_btnTest.Text = "Test";
            this.pnl2_btnTest.UseVisualStyleBackColor = true;
            this.pnl2_btnTest.Click += new System.EventHandler(this.btnTest_Click);
            this.pnl2_btnTest.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl2_Color3
            // 
            this.pnl2_Color3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl2_Color3.Location = new System.Drawing.Point(137, 16);
            this.pnl2_Color3.Name = "pnl2_Color3";
            this.pnl2_Color3.Size = new System.Drawing.Size(58, 47);
            this.pnl2_Color3.TabIndex = 8;
            this.pnl2_Color3.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl2_Color3.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // pnl2_Color2
            // 
            this.pnl2_Color2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl2_Color2.Location = new System.Drawing.Point(73, 16);
            this.pnl2_Color2.Name = "pnl2_Color2";
            this.pnl2_Color2.Size = new System.Drawing.Size(58, 47);
            this.pnl2_Color2.TabIndex = 3;
            this.pnl2_Color2.Click += new System.EventHandler(this.pnlColor_Click);
            this.pnl2_Color2.MouseHover += new System.EventHandler(this.pnl_Color_MouseHover);
            // 
            // FrmDecodingBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 654);
            this.Controls.Add(this.pnl1);
            this.Controls.Add(this.pnl2);
            this.Controls.Add(this.pnl3);
            this.Controls.Add(this.pnl4);
            this.Controls.Add(this.pnl5);
            this.Controls.Add(this.pnl0);
            this.Controls.Add(this.pnlSeperator);
            this.Controls.Add(this.btnShowHide);
            this.Controls.Add(this.pnl6);
            this.Name = "FrmDecodingBoard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mastermind";
            this.Load += new System.EventHandler(this.FrmDecodingBoard_Load);
            this.pnl6.ResumeLayout(false);
            this.pnl0.ResumeLayout(false);
            this.pnl5.ResumeLayout(false);
            this.pnl3.ResumeLayout(false);
            this.pnl4.ResumeLayout(false);
            this.pnl1.ResumeLayout(false);
            this.pnl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

      
        #endregion

        private System.Windows.Forms.Panel pnl6;
        private System.Windows.Forms.Button pnl6_btnTest;
        private System.Windows.Forms.Button btnShowHide;
        private System.Windows.Forms.Panel pnlSeperator;
        private System.Windows.Forms.Panel pnl6_Status3;
        private System.Windows.Forms.Panel pnl6_Status2;
        private System.Windows.Forms.Panel pnl6_Status1;
        private System.Windows.Forms.Panel pnl6_Color1;
        private System.Windows.Forms.Panel pnl6_Color3;
        private System.Windows.Forms.Panel pnl6_Color2;
        private System.Windows.Forms.Panel pnl0;
        private System.Windows.Forms.Panel pnl0_Color1;
        private System.Windows.Forms.Panel pnl0_Color3;
        private System.Windows.Forms.Panel pnl0_Color2;
        private System.Windows.Forms.Panel pnl5;
        private System.Windows.Forms.Panel pnl5_status3;
        private System.Windows.Forms.Panel pnl5_Status2;
        private System.Windows.Forms.Panel pnl5_Status1;
        private System.Windows.Forms.Panel pnl5_Color1;
        private System.Windows.Forms.Button pnl5_btnTest;
        private System.Windows.Forms.Panel pnl5_Color3;
        private System.Windows.Forms.Panel pnl5_Color2;
        private System.Windows.Forms.Panel pnl3;
        private System.Windows.Forms.Panel pnl3_Status3;
        private System.Windows.Forms.Panel pnl3_Status2;
        private System.Windows.Forms.Panel pnl3_Status1;
        private System.Windows.Forms.Panel pnl3_Color1;
        private System.Windows.Forms.Button pnl3_btnTest;
        private System.Windows.Forms.Panel pnl3_Color3;
        private System.Windows.Forms.Panel pnl3_Color2;
        private System.Windows.Forms.Panel pnl4;
        private System.Windows.Forms.Panel pnl4_Status3;
        private System.Windows.Forms.Panel pnl4_Status2;
        private System.Windows.Forms.Panel pnl4_Status1;
        private System.Windows.Forms.Panel pnl4_Color1;
        private System.Windows.Forms.Button pnl4_btnTest;
        private System.Windows.Forms.Panel pnl4_Color3;
        private System.Windows.Forms.Panel pnl4_Color2;
        private System.Windows.Forms.Panel pnl1;
        private System.Windows.Forms.Panel pnl1_Status3;
        private System.Windows.Forms.Panel pnl1_Status2;
        private System.Windows.Forms.Panel pnl1_Status1;
        private System.Windows.Forms.Panel pnl1_Color1;
        private System.Windows.Forms.Button pnl1_btnTest;
        private System.Windows.Forms.Panel pnl1_Color3;
        private System.Windows.Forms.Panel pnl1_Color2;
        private System.Windows.Forms.Panel pnl2;
        private System.Windows.Forms.Panel pnl2_Status3;
        private System.Windows.Forms.Panel pnl2_Status2;
        private System.Windows.Forms.Panel pnl2_Status1;
        private System.Windows.Forms.Panel pnl2_Color1;
        private System.Windows.Forms.Button pnl2_btnTest;
        private System.Windows.Forms.Panel pnl2_Color3;
        private System.Windows.Forms.Panel pnl2_Color2;
    }
}

