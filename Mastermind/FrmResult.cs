﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mastermind
{
    public partial class FrmResult : Form
    {
        public FrmResult(bool Win)
        {
            InitializeComponent();
            if (Win)
            {
                lblResult.Text = "You Win!";
                this.Text = "You Win!";
                pictureBox1.ImageLocation = @"..\..\Images\Win.jpg";
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else
            {
                lblResult.Text = "You Lose!";
                this.Text = "You Lose!";
                pictureBox1.ImageLocation = @"..\..\Images\Lose.jpg";
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void FrmResult_Load(object sender, EventArgs e)
        {
            
        }
    }
}
