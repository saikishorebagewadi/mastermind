﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mastermind
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            IColorDefinition colorDefinition = new ColorDefinition();
            ITestUserSelections testUserSelections = new TestUserSelections();
            ISelectComputerColors selectComputerColors = new SelectComputerColors();
            Application.Run(new FrmDecodingBoard(colorDefinition, testUserSelections, selectComputerColors));
        }
    }
}
